package ru.obninsk.iate.edu.ontology;

import java.util.Scanner;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.shared.JenaException;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by abakumov on 18.05.2017.
 */
public class Console {

    private static final Logger LOGGER = LoggerFactory.getLogger(Console.class);

    private final OntModel model;
    private QueryExecutor queryExecutor;

    public Console(OntModel model) {
        this.model = model;
        queryExecutor = new QueryExecutor(model);
    }


    public void run() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print('>');
            String command = scanner.nextLine();
            if("exit".equals(command)) {
                break;
            }
            if("list".equals(command)) {
                listClasses();
            } else {
                try {
                    String result = queryExecutor.executeAndPring(command);
                    LOGGER.info("SPARQL query: {} executed with result: {}", command, result);
                    System.out.println(result);
                } catch (JenaException je) {
                    je.printStackTrace();
                }
            }


        }
    }


    public void listClasses() {
        ExtendedIterator classes = model.listClasses();
        while (classes.hasNext()) {
            OntClass theClass = (OntClass) classes.next();
            String className = theClass.getLocalName();
            if (className != null) {
                String classInfo = OntologyClassPrinter.getClassInfo(theClass);
                LOGGER.info(classInfo);
                System.out.println(classInfo);
            }
        }
    }

}
