package ru.obninsk.iate.edu.ontology;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.util.iterator.ExtendedIterator;

/**
 * Created by abakumov on 17.05.2017.
 */
public class OntologyClassPrinter {

    public static String getClassInfo(OntClass ontClass) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Class Name: ").append(ontClass.getLocalName()).append('\n');
        OntClass superClass = ontClass.getSuperClass();
        if (superClass != null) {
            stringBuilder.append("Superclass: ").append(superClass).append('\n');
        }

        stringBuilder.append("Properties:\n");
        ExtendedIterator<OntProperty> ontPropertyExtendedIterator = ontClass.listDeclaredProperties();
        while (ontPropertyExtendedIterator.hasNext()) {
            OntProperty prop = ontPropertyExtendedIterator.next();
            stringBuilder.append("Property ").append(prop.getLocalName()).append(" domain: ").append(prop.getDomain())
                    .append(" range: ").append(prop.getRange()).append('\n');
        }

        return stringBuilder.toString();
    }
}
