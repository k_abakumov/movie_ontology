// @author Victor Telnov    Работа с Jena   
// Загрузка онтологии из файла и вывод имён всех классов
package ru.obninsk.iate.edu.ontology;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.ontology.*;

public class OntologyEditor extends Object {

    private static final Logger LOGGER = LoggerFactory.getLogger(OntologyEditor.class);

    public static void main(String[] args) {

        if(args.length == 0) {
            LOGGER.error("Ontology file not specified");
            System.out.println("Ontology parameter skipped. To run Ontology Editor specify ontology file name as input parameter");
        }

        OntModel ontModel = OntologyLoader.loadModelFromFile(args[0]);

        Console console = new Console(ontModel);
        console.run();
    }
}
