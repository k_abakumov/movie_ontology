package ru.obninsk.iate.edu.ontology;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;

/**
 * Created by abakumov on 18.05.2017.
 */
public class QueryExecutor {

    private final OntModel model;


    public QueryExecutor(OntModel model) {
        this.model = model;
    }

    public String executeAndPring(String queryString) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Query query = QueryFactory.create(queryString);

        QueryExecution qe = QueryExecutionFactory.create(query, model);
        try {
            ResultSet results = qe.execSelect();

            ResultSetFormatter.out(outputStream, results, query);
        } finally {
            qe.close();

        }
        return outputStream.toString();
    }

}
