package ru.obninsk.iate.edu.ontology;

import java.io.InputStream;
import org.apache.jena.ontology.OntDocumentManager;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.shared.JenaException;
import org.apache.jena.util.FileManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by abakumov on 17.05.2017.
 */
public class OntologyLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(OntologyLoader.class);

    public static OntModel loadModelFromFile(String filename) {
        OntDocumentManager mgr = new OntDocumentManager();
        OntModelSpec s = new OntModelSpec(OntModelSpec.OWL_MEM);
        s.setDocumentManager(mgr);
        OntModel ontoModel = ModelFactory.createOntologyModel(s);
        try {
            InputStream in = FileManager.get().open(filename);
            ontoModel.read(in, null);

            LOGGER.info("Ontology loaded from file: " + filename);
        } catch (JenaException je) {
            LOGGER.error("Error occurred while processing ontology: ", je);
        }
        return ontoModel;
    }

}
