# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

University project. Movies ontology editor and viewer
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Запускаем программу.
Главный класс - Ontology Editor

Программа работает в консольном режиме

Поддерживаемые команды:
exit - выйти
list - вывести все классы и их параметры
Любое другое вхождение будет трактоваться как SPARQL запрос


To understand OWL model visit [Jena Ontology API Guide](https://jena.apache.org/documentation/ontology/)

To understand SPARQL visit [SPARQL Tutorial](https://jena.apache.org/tutorials/sparql_pt.html)

Queries examples:

```
#!SPARQL
#Selects all class - subclass relations

SELECT ?x ?fname WHERE {?x  <http://www.w3.org/2000/01/rdf-schema#subClassOf>  ?fname}
```

### Building and running ###
To build the program use maven. Launch the following in the root directory:


```
#!Batch

mvn clean install
```

This will create executable jar in target directory with name **movies_ontology-1.0-SNAPSHOT-jar-with-dependencies.jar**

To run the program launch this jar with ontology file name as launch argument:

```
#!Batch

java -jar target/movies_ontology-1.0-SNAPSHOT-jar-with-dependencies.jar data/movie_ontology.owl
```